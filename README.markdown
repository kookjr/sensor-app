# Sensor Application

# Development
## Source Repository

## Build Source

# Running
## Usage

    sensor_app [-dh] [-a addr] [-p port] [-t topic_root]
        -d            - Enable syslog LOG_DEBUG messages
        -h            - print help and exit
        -p port       - MQTT destination port
        -a addr       - MQTT destination address/host
        -h topic_root - root of topic usually machine ID

## Systemd Configuration

The sensor app can be started at boot time. For example here is a
systemd unit file.

    [Unit]
    Description=MQTT sensor service
    After=network-online.target
    Wants=network-online.target

    [Service]
    Environment=MQTT_BROKER_HOST=192.168.0.106
    ExecStartPre=/bin/sh -c 'until ping -c1 ${MQTT_BROKER_HOST}; do sleep 1; done;'
    # set command line options for your setup, probably use
    #   -a address ip address/name of MQTT broker
    #   -p port    ip port of MQTT broker
    #   -t topic   root of topic, e.g. kitchenpc
    ExecStart=/usr/local/bin/sensor_app -a ${MQTT_BROKER_HOST} -t pi

The extra ExecStartPre is required because there is a big enough delay
after the network is online to when you can connect to an outside
target.

Edit the MQTT_BROKER_HOST to point to your broker.

## One Time Systemd Setup

This procedure was used on a Raspberry Pi Zero. Copy the
sensorapp.service file to the Raspbery Pi and perfrom the following to
install it.

    sudo cp sensorapp.service /lib/systemd/system/sensorapp.service
    sudo chmod 644 /lib/systemd/system/sensorapp.service
    sudo systemctl enable systemd-networkd.service systemd-networkd-wait-online.service
    sudo systemctl daemon-reload
    sudo systemctl enable sensorapp.service
    sudo reboot

If you need to make edits to the unit file and retry it.

    # edit the local copy
    sudo cp sensorapp.service /lib/systemd/system/sensorapp.service
    sudo systemctl daemon-reload # restart all, regen deps
    sudo systemctl restart sensorapp.service
    sudo systemctl status sensorapp.service
    # sudo reboot to make sure the startup works too

# License

BSD-style license. For details, see "COPYING" file.
