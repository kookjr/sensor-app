// Copyright (c) 2021 Mathew Cucuzella (kookjr@gmail.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior written
//       permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <cstring>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <syslog.h>

#include <unistd.h>
#include <fcntl.h>
#include <glob.h>

#include "thermal_zones.hpp"

void thermal_zones::discover_zones() {
    // /sys/class/thermal/thermal_zone*/{temp,type}
    glob_t g{0,nullptr,0};

    int r = glob("/sys/class/thermal/thermal_zone*",
                 GLOB_ERR,
                 nullptr,
                 &g);
    if (r == GLOB_NOMATCH)
        return;

    // need to cd there, or basename the paths, and maybe remove the
    // ending slash
    // match: /sys/class/thermal/thermal_zone0/
    // match: /sys/class/thermal/thermal_zone1/
    // match: /sys/class/thermal/thermal_zone2/
    // match: /sys/class/thermal/thermal_zone3/
    // match: /sys/class/thermal/thermal_zone4/

    for (size_t i=0; i < g.gl_pathc; ++i) {
        auto p = strrchr(g.gl_pathv[i], '/');
        zones.emplace_back((p == nullptr) ? g.gl_pathv[i] : p + 1);
    }

    globfree(&g);
}

bool thermal_zones::refresh_data() {
    if (zones.empty())
        discover_zones();

    char cnt{'0'};
    zinfo.clear();

    // loop through zones and get name/temp
    std::for_each(
        std::begin(zones),
        std::end(zones),
        [this, &cnt](const std::string& zone) {
            std::string zone_temp = file_to_string(
                std::string("/sys/class/thermal/") + zone + "/temp");
            std::string zone_type = file_to_string(
                std::string("/sys/class/thermal/") + zone + "/type");
            if (!zone_type.empty() && !zone_temp.empty()) {
                zone_type.push_back('_');
                zone_type.push_back(cnt++);
                zinfo.emplace_back(zone_type, zone_temp);
            }
        }
    );

    return true;
}

std::string thermal_zones::file_to_string(const std::string& file) {
    std::string contents;

    int fd;
    if ( (fd=open(file.c_str(), O_RDONLY)) != -1 ) {
        char buf[128];
        int cnt;
        if ( (cnt=read(fd, buf, sizeof(buf))) > 0 ) {
            // erase ending white space
            while (cnt && std::isspace(buf[cnt-1]))
                --cnt;
            contents = std::move(std::string(buf, cnt));
        }
        close(fd);
    }
    return contents;
}

std::vector<thermal_zones::zoneinfo> thermal_zones::get_data() {
    return zinfo;
}

// millidegress C
// C to F = (c * 9/5) +32
float thermal_zones::millidegress_to_degrees(const std::string& mdeg) {
    auto ival = std::stoi(mdeg) / 1000.0;
    return ((ival * 9) / 5) + 32.0;
}
std::string thermal_zones::millidegress_to_degrees_str(const std::string& mdeg) {
    std::stringstream ss;
    ss << std::fixed
       << std::setprecision(1)
       << millidegress_to_degrees(mdeg);
    return ss.str();
}
