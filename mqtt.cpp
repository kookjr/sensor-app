// Copyright (c) 2021 Mathew Cucuzella (kookjr@gmail.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior written
//       permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <syslog.h>
#include <unistd.h>
#include "globalcfg.hpp"
#include "mqtt.hpp"

mqtt::mqtt() :
    publisher(),
    mosq(nullptr),
    connected(false) {
}

mqtt::~mqtt() {
    if (mosq) {
        if (connected) {
            mosquitto_disconnect(mosq);
        }
        mosquitto_destroy(mosq);
    }
    mosquitto_lib_cleanup();
}

const char* mqtt::setup(const retrytimes& rt) {
    int mosq_err;

    if (mosquitto_lib_init() != MOSQ_ERR_SUCCESS)
        return mosquitto_strerror(mosq_err);

    int M{0}, m{0}, r{0};
    mosquitto_lib_version(&M, &m, &r);
    syslog(LOG_INFO, "mosquitto version %d.%d.%d", M, m, r);

    mosq = mosquitto_new(
        nullptr, // choose random client id
        true,    // clean session at startup
        this);   // user data ptr for callbacks
    if (mosq == nullptr)
        return "client instance alloc";

    mosquitto_log_callback_set(mosq, lib_logger);
    mosquitto_disconnect_callback_set(mosq, lib_disconnect);

    auto retry_times{rt};

    do {
        mosq_err = mosquitto_connect(
            mosq,
            globalcfg::instance().ipaddr().c_str(),
            globalcfg::instance().port(),
            30);

        if (mosq_err == MOSQ_ERR_SUCCESS) {
            syslog(LOG_INFO, "connected to broker");
            break;
        }

        sleep(retry_times.next());
    }
    while (true);

    if (mosq_err != MOSQ_ERR_SUCCESS)
        return mosquitto_strerror(mosq_err);

    connected = true;

    return nullptr;
}

bool mqtt::poll_mqtt() {
    if (! connected)
        return false;

    int r = mosquitto_loop(mosq, 0, 1);

    if (r != MOSQ_ERR_SUCCESS)
        syslog(LOG_WARNING, "mqtt poll: %s", mosquitto_strerror(r));

    return (r == MOSQ_ERR_SUCCESS);
}

bool mqtt::mqtt_publish(const std::string& topic, const std::string& temperature) {
    auto full_topic = globalcfg::instance().topic_root() + "/temp/" + topic;
    int r = mosquitto_publish(
        mosq,
        nullptr,
        full_topic.c_str(),
        temperature.size() + 1, // +1 for null term
        temperature.c_str(),
        0,
        0);

    if (r == MOSQ_ERR_SUCCESS)
        return true;

    syslog(LOG_WARNING, "mqtt publish: %s", mosquitto_strerror(r));

    return false;
}

void mqtt::disconnect_event(int rc) {
    syslog(LOG_WARNING, "broker disconnected");
    connected = false;
}

void mqtt::lib_logger(struct mosquitto*, void*, int, const char* msg) {
    if (msg)
        syslog(LOG_DEBUG, "MQTT Status: %s", msg);
}

void mqtt::lib_disconnect(struct mosquitto*, void* obj, int rc) {
    auto mp = static_cast<mqtt*>(obj);
    mp->disconnect_event(rc);

    if (rc != MOSQ_ERR_SUCCESS)
        syslog(LOG_DEBUG, "broker disconnected");
}
