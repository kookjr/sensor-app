// Copyright (c) 2021 Mathew Cucuzella (kookjr@gmail.com)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior written
//       permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <stdio.h>
#include <unistd.h>
#include <syslog.h>
#include "globalcfg.hpp"

namespace {
    char const * const C_OPTSTR = "dp:a:t:h";
}

globalcfg& globalcfg::instance() {
    static globalcfg instance;
    return instance;
}

bool globalcfg::set_clargs(int argc, char* argv[]) {
    opterr = 0; // disable error printing

    enum class gus { dont, help_ok, error };
    gus give_up = gus::dont;
    int opt;
    while ( give_up==gus::dont && (opt=getopt(argc, argv, C_OPTSTR)) != -1 ) {
        switch (opt) {
        case 'd':
            c_debug = true;
            break;
        case 'p':
            c_port = atoi(optarg);
            break;
        case 'a':
            c_ipaddr = std::string(optarg);
            break;
        case 't':
            c_topic = std::string(optarg);
            break;
        case 'h':
            printf("Usage: %s [-dh] [-a addr] [-p port] [-t topic_root]\n",
                   argv[0]);
            give_up = gus::help_ok;
            break;
        default:
            syslog(LOG_WARNING, "invalid command line args");
            give_up = gus::error;
            break;
        }
    }
    // return true indicates if we want to continue running, false
    // means exit because an error or just printing usage
    return (give_up == gus::dont);
}

bool globalcfg::debug() const {
    return false;
}

std::string globalcfg::ipaddr() const {
    return c_ipaddr;
}

std::string globalcfg::topic_root() const {
    return c_topic;
}

int globalcfg::port() const {
    return c_port;
}
