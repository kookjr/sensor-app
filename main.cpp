// Copyright (c) 2021 Mathew Cucuzella (kookjr@gmail.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior written
//       permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <cstdlib>
#include <csignal>
#include <syslog.h>

#include "globalcfg.hpp"
#include "retrytimes.hpp"
#include "sensor_app.hpp"

static volatile sig_atomic_t got_exit_request = 0;

static void sighandler(int signo) {
    got_exit_request = 1;
}

int main(int argc, char* argv[]) {
    openlog("sensorapp", 0, LOG_USER);
    if (!globalcfg::instance().set_clargs(argc, argv)) {
        closelog();
        return EXIT_SUCCESS;
    }

    if (globalcfg::instance().debug())
        setlogmask(LOG_UPTO(LOG_DEBUG));
    else
        setlogmask(LOG_UPTO(LOG_INFO));

    struct sigaction sa{};
    sa.sa_handler = &sighandler;
    if (sigaction(SIGINT, &sa, NULL) == -1)
        syslog(LOG_WARNING, "trouble setting up signal handler");

    sensor_app app;

    // add all sensor classes here
    //   see sensor.hpp for how to add new sensors
    app.load_sensors();

    // initialize everything then enter processing loop
    if (app.init_transport(retrytimes(1,5*60)) && app.init_sensors())
        app.sensor_loop(got_exit_request);

    syslog(LOG_INFO, "exiting");
    closelog();

    // destructors cleanup
    return EXIT_SUCCESS;
}
