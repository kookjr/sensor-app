// Copyright (c) 2021 Mathew Cucuzella (kookjr@gmail.com)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior written
//       permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef SENSOR_APP_HPP
#define SENSOR_APP_HPP

#include <csignal>
#include <ctime>

#include <array>
#include <memory>

#include "sensor.hpp"
#include "mqtt.hpp"
#include "retrytimes.hpp"

class sensor_app {
    static constexpr int MQTT_MAX_INTERVAL_MS = 1500;
    std::array<std::unique_ptr<sensor>,1> sensors;
    mqtt mqttimpl;

public:
    sensor_app()  = default;
    ~sensor_app() = default;

    void load_sensors();

    // retry connects/reconnects forever at rt intervals
    bool init_transport(const retrytimes& rt);
    bool init_sensors();

    void sensor_loop(volatile sig_atomic_t& req_cancel_flag);
private:
    struct timespec* ms_to_ts(struct timespec* ts, long ms) const;
};

#endif // SENSOR_APP_HPP
