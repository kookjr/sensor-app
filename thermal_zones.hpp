// Copyright (c) 2021 Mathew Cucuzella (kookjr@gmail.com)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior written
//       permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef THERMAL_ZONES_HPP
#define THERMAL_ZONES_HPP

#include <string>
#include <vector>

class thermal_zones {
public:
    struct zoneinfo {
        zoneinfo()  = default;
        ~zoneinfo() = default;
        zoneinfo(const std::string& s, const std::string& f) : name(s), temp_millideg_C(f) {};

        std::string name;
        std::string temp_millideg_C;
    };

    thermal_zones()  = default;
    ~thermal_zones() = default;

    bool refresh_data();
    std::vector<zoneinfo> get_data();

    static float millidegress_to_degrees(const std::string& mdeg);
    static std::string millidegress_to_degrees_str(const std::string& mdeg);

private:
    std::vector<std::string> zones;
    std::vector<zoneinfo> zinfo;

    void discover_zones();
    std::string file_to_string(const std::string& file);
};

#endif // THERMAL_ZONES_HPP
