// Copyright (c) 2021 Mathew Cucuzella (kookjr@gmail.com)
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior written
//       permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <ctime>
#include <syslog.h>

#include <algorithm>

#include "thermalzone_sensor.hpp"
#include "sensor_app.hpp"

void sensor_app::load_sensors() {
    sensors[0] = std::make_unique<thermalzone_sensor>();
}

bool sensor_app::init_transport(const retrytimes& rt) {
    const char* msg = mqttimpl.setup(rt);
    if (msg) {
        syslog(LOG_WARNING, "setup MQTT failure: %s", msg);
        return false;
    }
    return true;
}

bool sensor_app::init_sensors() {
    std::for_each(begin(sensors), end(sensors), [](std::unique_ptr<sensor>& s) {
        s->initialize();
        }
    );
    return true;
}

void sensor_app::sensor_loop(volatile sig_atomic_t& req_cancel_flag) {
    struct timespec ts{0,0};
    long interval{MQTT_MAX_INTERVAL_MS};

    while (!req_cancel_flag) {
        if (! mqttimpl.poll_mqtt())
            break;

        nanosleep(ms_to_ts(&ts, interval), nullptr);

        long next_interval{MQTT_MAX_INTERVAL_MS};
        std::for_each(begin(sensors), end(sensors), [&](std::unique_ptr<sensor>& s) {
            if (s->elapsed_time(interval)) {
                s->run_once(mqttimpl);
            }
            next_interval = std::min(s->time_left(), next_interval);
            }
        );

        interval = next_interval;
    }
}

struct timespec* sensor_app::ms_to_ts(struct timespec* ts, long ms) const {
    if (ts) {
        ts->tv_sec  = (ms / 1000L);
        ts->tv_nsec = (ms % 1000L) * 1000000;
    }

    return ts;
}
