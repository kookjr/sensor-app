// Copyright (c) 2021 Mathew Cucuzella (kookjr@gmail.com)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior written
//       permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef MQTT_HPP
#define MQTT_HPP

#include <string>

#include "mosquitto.h"

#include "publisher.hpp"
#include "retrytimes.hpp"

class mqtt : public publisher {
    struct mosquitto* mosq;
    bool connected;

public:
    mqtt();
    ~mqtt();

    // retry connects/reconnects forever with intervals based on rt
    const char* setup(const retrytimes& rt);
    // do mqtt work
    bool poll_mqtt();
    // publish a message
    bool mqtt_publish(const std::string& topic, const std::string& temperature) override;
    // an external disconnect occurred
    void disconnect_event(int rc);

    // library callbacks to:
    //   log messages from the library
    //   monitor disconnect from broker
    static void lib_logger(struct mosquitto*, void*, int, const char* msg);
    static void lib_disconnect(struct mosquitto*, void*, int);
};

#endif // MQTT_HPP
