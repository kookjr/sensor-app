#!/bin/bash

# Usage: ./mk [Debug|Release|...]

BLDDIR=b

if [ -d ${BLDDIR} ]
then
  echo "remove build dir ${BLDDIR} (y or n)?"
  read reply
  if [ "${reply}" = "y" ]
  then
    rm -rf ${BLDDIR}
  else
    echo "exiting..."
    exit 1
  fi
fi

mkdir ${BLDDIR} && cd ${BLDDIR} \
  && cmake -DCMAKE_BUILD_TYPE=${1:-MinSizeRel} .. \
  && make -j$(nproc) VERBOSE=1
